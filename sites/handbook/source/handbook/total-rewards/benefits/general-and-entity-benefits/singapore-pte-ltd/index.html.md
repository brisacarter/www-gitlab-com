---
layout: handbook-page-toc
title: "GitLab Singapore Pte Ltd"
description: "Discover GitLab's benefits for team members in Singapore"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to team members in Singapore

For any questions related to Singapore benefits, please email `total-rewards@gitlab.com` for further assistance.

### Statutory Leave Entitlements

**Annual Leave** 
- Team members in Singapore are entitled to Fourteen (14) days of annual leave per year which accrues on a pro rata basis throughout the year. This leave runs concurrently with GitLab PTO. Team members should initiate leave by entering their time off into PTO by Roots and selecting the `Vacation` option.
- Team members must coordinate with their team prior to taking time off to ensure business continuity. The Company may in its absolute discretion rescind its approval for any leave applied for where the exigencies of work so require.
- Unused annual leave may not be carried forward from any year to a succeeding year except with the prior approval of the Company.
- Unless the Company approves or requires otherwise, annual leave may not be used to offset any notice periods related to a team member's resignation or termination.

**Other Leave**
- Team members in Singapore are entitled to such outpatient sick leave and hospitalization leave as provided in the Employment Act.
- Your entitlement (if any) to maternity leave and benefits, adoption leave, childcare leave, extended childcare leave, unpaid infant care leave, shared parental leave and paternity leave shall be as provided in the Child Development Co-Savings Act and the Employment Act.
