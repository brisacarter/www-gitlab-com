---
layout: handbook-page-toc
title: Security Awards Program Runbook
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Award nominations

Every Monday morning:

- [Find the open Security Awards Council Issues](https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues?label_name[]=Security%20Awards%20Council). There should be only one open for the past week.
- For every nomination (comment thread in the thread)
  -  If the number of votes (thumbs up / :+1:) if > 2
     - Set the label `~security-awards::awarded` in the nominated issue or Merge Request
     - Add a comment with the text: `Congratulations :tada: @<author>, your Issue/Merge Request has been [awarded](https://gitlab.com/gitlab-com/security-awards/-/merge_requests/<merge request ID>/diffs) by the AppSec team. ([Learn more about the Security Awards Program](https://about.gitlab.com/handbook/engineering/security/security-awards-program.html))` (Be careful to replace `<author>` and `merge request id`).
     - Add the award to the [awards data file](data/awards.yml). Points = number of votes (:+1: in the threaded discussion) * 100.
  -  Else
     - Set the label `~security-awards::rejected` in the nominated issue or Merge Request.
- Close the Security Awards Council Issue
- If some nominations were awarded (file [awards.yml](data/awards.yml) updated)
  - Create a merge request to generate a new leaderboard.
  - Update the [README.md](README.md) file with the generated leaderboard (job `generate leaderboard` in the corresponding pipeline).
- Create a [new Council issue, with this template](https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/new?issuable_template=Awards%20Council)
- Fetch new nominations (see [Fetch nominated Issues and Merge Requests](#fetch-nominated-issues-and-merge-requests) below).
- For every new nomination
  - Add a threaded comment with the text:   

```markdown
### [title of the issue or merge request](link to issue or merge request)

Nominated by @<someone from the appsec team>
```

## The Security Awards CLI

The easiest way to run the cli tool is to use the docker image provided in this [repo](https://gitlab.com/gitlab-com/security-awards/):

```shell
alias awards="docker run -it --rm -v \`pwd\`/data:/data/ registry.gitlab.com/gitlab-com/security-awards"
```

or if you don't want to use `docker`:

```shell
cd cli && go build -o awards
```

### Fetch nominated Issues and Merge Requests

```shell
 export GITLAB_API_TOKEN="a gitlab api token"
awards fetch
```

### Generate the leaderboard

```shell
awards generate leaderboard
```

This will generate a markdown leaderboard to be copied in this [README.md](https://gitlab.com/gitlab-com/security-awards/-/blob/master/README.md) file.

This process is being automated, see https://gitlab.com/gitlab-com/gl-security/appsec/appsec-team/-/issues/84
