---
layout: markdown_page
title: "Sales Play: Atlassian Server EOL"
description: "This page has all the information for the Atlassian Server EOL sales play."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

### **Atlassian Server EOL Sales Play**

**[View this page in a Google Doc](https://docs.google.com/document/d/1ZTYFGDJN6t2fJ824kt3BvvicN3-qr0U5afVSGXubEzE/edit?usp=sharing)**

**Context:** What concerns, if any, does the customer have about this [Atlassian EOL announcement](https://www.atlassian.com/migration/journey-to-cloud) across different stages of the SDLC? Does this situation impact the prospect's instance of Jira, BitBucket (SCM) and/or Bamboo (CI)?


#### **Potential opportunities:**



1. They want to move off of BitBucket
2. They want to move off of Bamboo CI (or other e.g. Jenkins, TravisCI, CircleCI, etc.)
3. Pitch the value of a complete DevOps solution delivered as a single application
4. We integrate with Jira and/or can serve as an alternative (if there is pain around Jira); this one is the most difficult since Jira is very “sticky” and feature-robust

<table>
  <tr>
   <td>

<h4><strong>Part 1: Before the Discovery Call</strong></h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Familiarize yourself with the following: 
<ul>
 
<li><a href="https://www.atlassian.com/migration/journey-to-cloud">Atlassian Server EOL Opportunity</a>
 
<li><a href="https://drive.google.com/file/d/18H73Fkh-cbPd5M-z0hMuGOx_onWIhSo4/view?usp=sharing">GitLab vs. Atlassian Battles</a> (watch at least first 10 min)
 
<li><a href="https://docs.google.com/presentation/d/1ydOqmyQmg0WFlmexPHxoPArv3SM5iFmX__0dV4HD0o0/edit?usp=sharing">GitLab vs. Atlassian Comparison Deck</a>
 
<li><a href="https://docs.google.com/presentation/d/15zyK_SN7S_eBfzkqOH4BTHd-M452ITFHs5RFtpj79L8/edit?usp=sharing">Mid Market Example Deck</a>
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Talk to the right persona 
<ul>
 
<li>The<strong> CIO</strong> is usually the <strong>economic buyer</strong>.
 
<li>The<strong> App Dev <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc">Exec</a>/<a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#dakota---the-application-development-director">Dir</a>/<a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager">Mgr</a><em> </em></strong>is interested in functionality and can <strong>champion</strong> GitLab internally.
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

3.  Review <a href="https://docs.google.com/document/d/1KDb6wHjanyC805OD1my60wH9Yh5qaq5895KOKDVazXY/edit">discovery questions, objections and differentiators</a>.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

4.  Create a custom pitch deck using these <a href="https://docs.google.com/presentation/d/1ydOqmyQmg0WFlmexPHxoPArv3SM5iFmX__0dV4HD0o0/edit?usp=sharing">slides</a>.
</li>
</ol>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>
<h4><strong>Part 2: The Discovery Call</strong></h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Identify <strong>business objectives</strong> and priorities during discovery. These could be: 
<ul>
 
<li>One platform for easier collaboration across the organization and especially with a distributed workforce.
 
<li>Cost savings - ability to eliminate multiple tools in favor of GitLab (and ability to seamlessly integrate if they don’t want to completely rip and replace).
 
<li>Higher levels of security and compliance to avoid breaches and associated risk.
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  After your call, follow up with an <a href="https://docs.google.com/document/d/14KDYzQ8HPaH4PW7ndRaBm8O2Z21da6bNp4TRJqnnXmQ/edit?usp=sharing">email</a>, which should include: 
<ul>
 
<li>Any information they requested (resources you might include)
 
<li>How that information can be helpful for their research
 
<li>An offer to continue researching these topics on their behalf
 
<li>Mutually agreed upon next steps.
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>
<h4><strong>Part 3: POV & Evaluation</strong></h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Set up the evaluation with the appropriate internal resources.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Before sending out a trial key: 
<ul>
 
<li>Mutually agree upon “<strong>success criteria</strong>” for the 30-day evaluation period
 
<li>“If at the end of 30-days, we prove [these criteria], then we will consider this trial a success.” Examples of success criteria include:  
<ol>
  
<li>Establish end to end DevSecOps workflow that improves cycle time and
  
<li>reduces Security & Compliance risk 
  
<li>If self-managed installation, ensure stable and scalable infrastructure
</li>  
</ol>
 
3.  Send an <a href="https://docs.google.com/document/d/14KDYzQ8HPaH4PW7ndRaBm8O2Z21da6bNp4TRJqnnXmQ/edit#bookmark=id.5grp5yaens0e">email</a> outlining and confirming the success criteria.
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

4.  Work with your SA to showcase a technical demo of how GitLab trumps Atlassian and/or share technical demos, webinars, and other resources.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

5.  Share <strong>customer stories</strong> and <a href="https://www.gartner.com/reviews/markets">Gartner Peer Insights</a>. 
<ul>
 
<li>Case Studies Filtered by:   
<ol>
  
<li><a href="https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&label_name[]=Replaces%20-%20Bitbucket">Replacing BitBucket</a>
  
<li><a href="https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&label_name[]=Replaces%20-%20JIRA">Replacing JIRA</a>
  
<li><a href="https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&label_name[]=Competitor%2FIntegration%20-%20JIRA">JIRA Integrations</a>
</li>  
</ol>
</li>  
</ul>
</li>  
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

6.  After each call during the trial period, send an <a href="https://docs.google.com/document/d/14KDYzQ8HPaH4PW7ndRaBm8O2Z21da6bNp4TRJqnnXmQ/edit#bookmark=id.6tok4lob9uox">email</a> with the progress achieved against the success criteria and what should be worked on the following week. 
</li>
</ol>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>
<h4><strong>Part 4: Negotiation / Decision-Making</strong></h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Use the “How GitLab Does it Better” section as supporting material to build the business case for GitLab (ROI) with the customer in the custom deck for the champion present to the economic buyer.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Share the <a href="https://drive.google.com/file/d/1Vi3_InvTs8r6cLvC4gR9bBumlWV5TNvY/view">Forrester TEI report</a>.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

3.  Use the <a href="https://about.gitlab.com/calculator/">Cost Comparison Calculators</a> to show the value of using GitLab as a single application over other solutions.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<h4><strong>Part 5: Close the Deal!</strong></h4>


   </td>
  </tr>
</table>
